package com.rabbit.controller;


import ch.qos.logback.core.net.SyslogOutputStream;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.area.AreaDto;
import com.rabbit.dto.area.AreaSearchDto;
import com.rabbit.entity.Area;
import com.rabbit.entity.Cust;
import com.rabbit.service.IAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.rabbit.controller.BaseController;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/area")
public class AreaController extends BaseController {
    @Autowired
    IAreaService areaService;
    //查全部客户
    @GetMapping("/list")
    public List<Area> getAll() {
        return this.areaService.list();
    }
    //根据动态条件查询客户列表, 且还要统计列名信息
    @GetMapping("/search")
    public PageSerializable<AreaDto> search(AreaSearchDto areaSearchDto) {
        return this.areaService.search(areaSearchDto);
    }

    @GetMapping("{id}")
    public Area getById(@PathVariable Serializable id){
        return areaService.getById(id);
    }

    @GetMapping("/of/{custId}")
    public List<Area> getByCustId(@PathVariable Serializable custId){
        return areaService.lambdaQuery().eq(Area::getCustId, custId).list();
    }

    //增加功能
    @PostMapping
    public boolean postInstall(@RequestBody com.rabbit.dto.AreaDto areaDto) {
        return this.areaService.save(areaDto);
    }
    //修改功能
    @PutMapping
    public boolean update(@RequestBody com.rabbit.dto.AreaDto areaDto ) {
        return this.areaService.updateById(areaDto);
    }
    //删除功能
    @DeleteMapping
    public boolean removeById(@PathVariable Serializable id) {
        return areaService.removeById(id);
    }

}
