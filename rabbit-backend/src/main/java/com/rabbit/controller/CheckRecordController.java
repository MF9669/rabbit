package com.rabbit.controller;

import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.CheckRecordDto;
import com.rabbit.dto.CheckRecordSearchDto;
import com.rabbit.entity.CheckRecord;
import com.rabbit.service.ICheckRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.rabbit.controller.BaseController;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 巡检记录 前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/check-record")
public class CheckRecordController extends BaseController {
    @Autowired
    ICheckRecordService checkRecordService;

    @GetMapping
    public List<CheckRecord> getAll() {
        return this.checkRecordService.list();
    }

    @GetMapping("/search")
    public PageSerializable<CheckRecordDto> search(CheckRecordSearchDto checkRecordSearchDto) {
        return this.checkRecordService.search(checkRecordSearchDto);
    }
}
