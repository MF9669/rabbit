package com.rabbit.controller;


import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.Cust.CustDto;
import com.rabbit.dto.Cust.CustSearchDto;
import com.rabbit.entity.Cust;
import com.rabbit.service.ICustService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 客户表 前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/cust")
public class CustController extends BaseController {
    @Autowired
    ICustService custService;
    @GetMapping("/list")
    public List<Cust> getAll(){
        return custService.list();
    }
    @GetMapping("/search")
    public PageSerializable<CustDto> search(CustSearchDto searchDto){
        return this.custService.search(searchDto);
    }

    @GetMapping("{id}")
    public Cust getOne(@PathVariable Serializable id){
        return custService.getById(id);
    }

    @PostMapping
    public boolean add(@RequestBody Cust cust){
        return custService.save(cust);
    }

    @DeleteMapping("{id}")
    public boolean removeById(@PathVariable Serializable id){
        return custService.removeById(id);
    }
    @PutMapping
    public boolean update(@RequestBody Cust cust){
        return custService.updateById(cust);
    }
}
