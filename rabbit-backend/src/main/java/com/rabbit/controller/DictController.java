package com.rabbit.controller;


import com.rabbit.entity.Cust;
import com.rabbit.entity.Dict;
import com.rabbit.service.IDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 数据字典 前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/dict")
public class DictController extends BaseController {
    @Autowired
    IDictService dictService;
@GetMapping("/list")
    public List<Dict> getAll(){
    return dictService.list();
}
@GetMapping("/search")
    public List<Dict>search(String name){
    return this.dictService.search(name);
}
@PostMapping
    public boolean add(@RequestBody Dict dict){
    return dictService.save(dict);
}
@DeleteMapping
    public boolean removeById(@PathVariable Serializable id){
    return dictService.removeById(id);
}
@PutMapping
    public boolean update(@RequestBody Dict dict){
    return dictService.updateById(dict);
}
}
