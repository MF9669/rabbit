package com.rabbit.controller;


import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.plc.PlcDto;
import com.rabbit.dto.plc.PlcSearchDto;
import com.rabbit.entity.Plc;
import com.rabbit.service.IPlcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/plc")
public class PlcController extends BaseController {
    @Autowired
    IPlcService plcService;

    @GetMapping("/list")
    public List<Plc> getAll(){
        return plcService.list();
    }

    @GetMapping("/search")
    public PageSerializable<PlcDto> search(PlcSearchDto plcSearch){
        return plcService.search(plcSearch);
   }

}
