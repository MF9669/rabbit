package com.rabbit.controller;


import com.rabbit.dto.PumpDto;
import com.rabbit.dto.PumpSearchDto;
import com.rabbit.entity.Pump;
import com.rabbit.service.IPumpService;
import com.rabbit.service.impl.PumpServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.rabbit.controller.BaseController;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/pump")
public class PumpController extends BaseController {
    @Autowired
    IPumpService pumpService;

    @GetMapping
    public List<Pump> getAll() {
        return this.pumpService.list();
    }

    @GetMapping("/search")
    public List<PumpDto> search(PumpSearchDto pumpSearchDto) {
        System.out.println(pumpSearchDto);
        return new ArrayList<>();
    }
}
