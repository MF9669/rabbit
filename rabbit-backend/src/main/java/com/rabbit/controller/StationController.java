package com.rabbit.controller;


import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.station.StationDto;
import com.rabbit.dto.station.StationSearchDto;
import com.rabbit.entity.Area;
import com.rabbit.entity.Station;
import com.rabbit.service.IStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/station")
public class StationController extends BaseController {

    @Autowired
    IStationService stationService;

    @GetMapping("/list")
    public List<Station> getAll(){
        return this.stationService.list();
    }

    @GetMapping("/search")
    public PageSerializable <StationDto> search(StationSearchDto stationSearchDto){
       return this.stationService.search(stationSearchDto);
    }

    @GetMapping("/search/{id}")
    public Station getById(@PathVariable Serializable id){
        return this.stationService.getById(id);
    }

    @GetMapping("/of/{custId}")
    public List<Area> getAreaByCustId(@PathVariable Long custId){
        return null;
    }




    @PutMapping
    public Boolean update(@RequestBody Station entity){
        return this.stationService.updateById(entity);
    }

    @DeleteMapping("{id}")
    public Boolean remove(@PathVariable Serializable id){
        return this.stationService.removeById(id);
    }

    @PostMapping
    public boolean add(@RequestBody Station station){
        return stationService.save(station);
    }

}
