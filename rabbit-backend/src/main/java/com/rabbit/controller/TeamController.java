package com.rabbit.controller;


import com.rabbit.entity.Team;
import com.rabbit.service.ITeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/team")
public class TeamController extends BaseController {
    @Autowired
    ITeamService teamService;
    @GetMapping("/list")
    public List<Team> getAll(){
        return teamService.list();
    }

    @GetMapping("/search")
    public List<Team> search(String name) {
        return this.teamService.search(name);
    }
    @PostMapping
    public boolean add(@RequestBody Team team){
        return teamService.save(team);
    }
    @DeleteMapping("{id}")
    public boolean removeById(@PathVariable Serializable id){
        return teamService.removeById(id);
    }
    @PutMapping
    public boolean update(@RequestBody Team team){
        return teamService.updateById(team);
    }
}
