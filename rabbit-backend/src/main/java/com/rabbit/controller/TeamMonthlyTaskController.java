package com.rabbit.controller;


import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.TeamMonthlyTask.TeamMonthlyTaskSearchDto;
import com.rabbit.entity.TeamMonthlyTask;
import com.rabbit.service.ITeamMonthlyTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 队伍月度任务 前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/team-monthly-task")
public class TeamMonthlyTaskController extends BaseController {
    @Autowired
    ITeamMonthlyTaskService teamMonthlyTaskService;
    @GetMapping("/list")
    public List<TeamMonthlyTask> getAll(){
        return teamMonthlyTaskService.list();}
    @GetMapping("/search")
     public PageSerializable<TeamMonthlyTask> search(TeamMonthlyTaskSearchDto searchDto){
        return this.teamMonthlyTaskService.search(searchDto);
        }
        @PostMapping
    public boolean add (@RequestBody TeamMonthlyTask teamMonthlyTask){
        return teamMonthlyTaskService.save(teamMonthlyTask);
        }
        @DeleteMapping
    public boolean removeById(@PathVariable Serializable id){
        return teamMonthlyTaskService.removeById(id);
        }
        @PutMapping
    public boolean update(@RequestBody TeamMonthlyTask teamMonthlyTask){
        return teamMonthlyTaskService.updateById(teamMonthlyTask);
        }
}
