package com.rabbit.controller;


import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.warn.WarnDto;
import com.rabbit.dto.warn.WarnSearchDto;
import com.rabbit.entity.Warn;
import com.rabbit.service.IWarnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 报警记录 前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/warn")
public class WarnController extends BaseController {

    @Autowired
    IWarnService warnService;

    @GetMapping
    public List<Warn> getAll(){
        return this.warnService.list();
    }

    @GetMapping("/search")
    public PageSerializable<WarnDto> search(WarnSearchDto warnSearchDto){
        return this.warnService.search(warnSearchDto);
    }

    @PutMapping
    public Boolean update(@RequestBody Warn entity){
        return this.warnService.updateById(entity);
    }

    @DeleteMapping("/{id}")
    public Boolean remove(@PathVariable Serializable id){
        return this.warnService.removeById(id);
    }

}
