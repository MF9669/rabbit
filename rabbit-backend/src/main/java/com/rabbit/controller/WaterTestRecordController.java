package com.rabbit.controller;


import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.waterTestRecord.WaterTestDto;
import com.rabbit.dto.waterTestRecord.WaterTestSearchDto;
import com.rabbit.entity.WaterTestRecord;
import com.rabbit.service.IWaterTestRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 水样检测记录 前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/water-test-record")
public class WaterTestRecordController extends BaseController {

    @Autowired
    IWaterTestRecordService waterTestRecordService;

    @GetMapping
    public List<WaterTestRecord> getAll(){
        return this.waterTestRecordService.list();
    }

    @GetMapping("/search")
    public PageSerializable<WaterTestDto> search(WaterTestSearchDto waterTestSearchDto){
        return this.waterTestRecordService.search(waterTestSearchDto);
    }


    @PutMapping
    public Boolean update(@RequestBody WaterTestRecord entity){
        return this.waterTestRecordService.updateById(entity);
    }

    @DeleteMapping("/{id}")
    public Boolean remove(@PathVariable Serializable id){
        return this.waterTestRecordService.removeById(id);
    }





}
