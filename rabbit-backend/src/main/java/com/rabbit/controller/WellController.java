package com.rabbit.controller;


import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.well.WellDto;
import com.rabbit.dto.well.WellSearchDto;
import com.rabbit.entity.Well;
import com.rabbit.service.IWellService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@RestController
@RequestMapping("/well")
public class WellController extends BaseController {

    @Autowired
    IWellService wellService;

    @GetMapping
    public List<Well> getAll(){
        return this.wellService.list();
    }


    @GetMapping("/search")
    public PageSerializable<WellDto> search(WellSearchDto wellSearchDto){
        return this.wellService.search(wellSearchDto);
    }

    @GetMapping("/search/{id}")
    public Well getById(@PathVariable Serializable id){
        return this.wellService.getById(id);
    }

    @PutMapping
    public Boolean update(@RequestBody Well entity){
        return this.wellService.updateById(entity);
    }

    @DeleteMapping("/{id}")
    public Boolean remove(@PathVariable Serializable id){
        return this.wellService.removeById(id);
    }

}
