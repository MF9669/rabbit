package com.rabbit.dto;

import com.rabbit.entity.CheckRecord;

public class CheckRecordDto extends CheckRecord {
    //所属客户的名称
    private String custName;
    //所属区域的名称
    private String areaName;
    //所属场站的名称
    private String stationName;
    //所属PLC的名称
    private String plcName;
    //所属泵的名称
    private String pumpName;
    //所属井的名称
    private String wellName;
    //巡检日期
    //巡检内容
    //场地清理情况
    //泵注速度
    //备注
    //创建人
    //创建日期

    public String getWellName() {
        return wellName;
    }

    public void setWellName(String wellName) {
        this.wellName = wellName;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getPlcName() {
        return plcName;
    }

    public void setPlcName(String plcName) {
        this.plcName = plcName;
    }

    public String getPumpName() {
        return pumpName;
    }

    public void setPumpName(String pumpName) {
        this.pumpName = pumpName;
    }
}
