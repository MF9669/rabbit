package com.rabbit.dto;

import java.util.Date;

public class CheckRecordSearchDto {
    private int pageNum = 1;

    private int pageSize = 10;

    private String kehu;

    private String quyu;

    private String cangzan;

    private String plc;

    private String ben;

    private String jin;

    private Date lo;

    private Date hi;

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "CheckRecordSearchDto{" +
                "kehu='" + kehu + '\'' +
                ", quyu='" + quyu + '\'' +
                ", cangzan='" + cangzan + '\'' +
                ", plc='" + plc + '\'' +
                ", ben='" + ben + '\'' +
                ", jin='" + jin + '\'' +
                ", lo=" + lo +
                ", hi=" + hi +
                '}';
    }

    public String getKehu() {
        return kehu;
    }

    public void setKehu(String kehu) {
        this.kehu = kehu;
    }

    public String getQuyu() {
        return quyu;
    }

    public void setQuyu(String quyu) {
        this.quyu = quyu;
    }

    public String getCangzan() {
        return cangzan;
    }

    public void setCangzan(String cangzan) {
        this.cangzan = cangzan;
    }

    public String getPlc() {
        return plc;
    }

    public void setPlc(String plc) {
        this.plc = plc;
    }

    public String getBen() {
        return ben;
    }

    public void setBen(String ben) {
        this.ben = ben;
    }

    public String getJin() {
        return jin;
    }

    public void setJin(String jin) {
        this.jin = jin;
    }

    public Date getLo() {
        return lo;
    }

    public void setLo(Date lo) {
        this.lo = lo;
    }

    public Date getHi() {
        return hi;
    }

    public void setHi(Date hi) {
        this.hi = hi;
    }
}
