package com.rabbit.dto.Cust;

import com.rabbit.entity.Cust;

public class CustDto extends Cust {
    private Long areaNum;
    private Long stationNum;
    private Long plcNum;
    private Long pumpNum;
    private Long wellNum;
    private Long replenishNum;
    private Long checkNum;
    private Long waterTestNum;
    private Long outNum;

    public Long getAreaNum() {
        return areaNum;
    }

    public void setAreaNum(Long areaNum) {
        this.areaNum = areaNum;
    }

    public Long getStationNum() {
        return stationNum;
    }

    public void setStationNum(Long stationNum) {
        this.stationNum = stationNum;
    }

    public Long getPlcNum() {
        return plcNum;
    }

    public void setPlcNum(Long plcNum) {
        this.plcNum = plcNum;
    }

    public Long getPumpNum() {
        return pumpNum;
    }

    public void setPumpNum(Long pumpNum) {
        this.pumpNum = pumpNum;
    }

    public Long getWellNum() {
        return wellNum;
    }

    public void setWellNum(Long wellNum) {
        this.wellNum = wellNum;
    }

    public Long getReplenishNum() {
        return replenishNum;
    }

    public void setReplenishNum(Long replenishNum) {
        this.replenishNum = replenishNum;
    }

    public Long getCheckNum() {
        return checkNum;
    }

    public void setCheckNum(Long checkNum) {
        this.checkNum = checkNum;
    }

    public Long getWaterTestNum() {
        return waterTestNum;
    }

    public void setWaterTestNum(Long waterTestNum) {
        this.waterTestNum = waterTestNum;
    }

    public Long getOutNum() {
        return outNum;
    }

    public void setOutNum(Long outNum) {
        this.outNum = outNum;
    }
}
