package com.rabbit.dto.Cust;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class CustSearchDto {
    private int pageNum = 1;
    private int pageSize = 10;
    private String name ;

    public Boolean getInService() {
        return inService;
    }

    public void setInService(Boolean inService) {
        this.inService = inService;
    }

    private Boolean inService;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date lo;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date hi;

    @Override
    public String toString() {
        return "CustSearchDto{" +
                "pageNum=" + pageNum +
                ", pageSize=" + pageSize +
                ", name='" + name + '\'' +
                ", lo=" + lo +
                ", hi=" + hi +
                '}';
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getLo() {
        return lo;
    }

    public void setLo(Date lo) {
        this.lo = lo;
    }

    public Date getHi() {
        return hi;
    }

    public void setHi(Date hi) {
        this.hi = hi;
    }
}
