package com.rabbit.dto;

import com.rabbit.entity.Pump;

import java.util.Date;

public class PumpDto extends Pump {
    //投运天数
    private Long touyunNum;

    public Long getTouyunNum() {
        return touyunNum;
    }

    public void setTouyunNum(Long touyunNum) {
        this.touyunNum = touyunNum;
    }
}
