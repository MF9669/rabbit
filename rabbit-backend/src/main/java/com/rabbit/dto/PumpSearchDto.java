package com.rabbit.dto;

import java.util.Date;

public class PumpSearchDto {
    private String benname;

    private String sosukehu;

    private String sosuquyu;

    private String sosucangzan;

    private String sosuplc;

    private String pumpleixn;

    private Boolean inService;

    private Date lo;

    public String getBenname() {
        return benname;
    }

    public void setBenname(String benname) {
        this.benname = benname;
    }

    public Boolean getInService() {
        return inService;
    }

    public void setInService(Boolean inService) {
        this.inService = inService;
    }

    public Date getLo() {
        return lo;
    }

    public void setLo(Date lo) {
        this.lo = lo;
    }

    public Date getHi() {
        return hi;
    }

    public void setHi(Date hi) {
        this.hi = hi;
    }

    @Override
    public String toString() {
        return "PumpSearchDto{" +
                "benname='" + benname + '\'' +
                ", sosukehu='" + sosukehu + '\'' +
                ", sosuquyu='" + sosuquyu + '\'' +
                ", sosucangzan='" + sosucangzan + '\'' +
                ", sosuplc='" + sosuplc + '\'' +
                ", pumpleixn='" + pumpleixn + '\'' +
                ", inService=" + inService +
                ", lo=" + lo +
                ", hi=" + hi +
                '}';
    }

    private Date hi;

    public String getSosukehu() {
        return sosukehu;
    }

    public void setSosukehu(String sosukehu) {
        this.sosukehu = sosukehu;
    }

    public String getSosuquyu() {
        return sosuquyu;
    }

    public void setSosuquyu(String sosuquyu) {
        this.sosuquyu = sosuquyu;
    }

    public String getSosucangzan() {
        return sosucangzan;
    }

    public void setSosucangzan(String sosucangzan) {
        this.sosucangzan = sosucangzan;
    }

    public String getSosuplc() {
        return sosuplc;
    }

    public void setSosuplc(String sosuplc) {
        this.sosuplc = sosuplc;
    }

    public String getPumpleixn() {
        return pumpleixn;
    }

    public void setPumpleixn(String pumpleixn) {
        this.pumpleixn = pumpleixn;
    }
}
