package com.rabbit.dto;

import java.util.Date;

public class ReplenishSearchDto {
    private String kehu;

    private String quyu;

    private String cangzan;

    private String duwu;

    private Date lo;

    private Date hi;

    @Override
    public String toString() {
        return "ReplenishSearchDto{" +
                "kehu='" + kehu + '\'' +
                ", quyu='" + quyu + '\'' +
                ", cangzan='" + cangzan + '\'' +
                ", duwu='" + duwu + '\'' +
                ", lo=" + lo +
                ", hi=" + hi +
                '}';
    }

    public String getKehu() {
        return kehu;
    }

    public void setKehu(String kehu) {
        this.kehu = kehu;
    }

    public String getQuyu() {
        return quyu;
    }

    public void setQuyu(String quyu) {
        this.quyu = quyu;
    }

    public String getCangzan() {
        return cangzan;
    }

    public void setCangzan(String cangzan) {
        this.cangzan = cangzan;
    }

    public String getDuwu() {
        return duwu;
    }

    public void setDuwu(String duwu) {
        this.duwu = duwu;
    }

    public Date getLo() {
        return lo;
    }

    public void setLo(Date lo) {
        this.lo = lo;
    }

    public Date getHi() {
        return hi;
    }

    public void setHi(Date hi) {
        this.hi = hi;
    }
}
