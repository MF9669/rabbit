package com.rabbit.dto.area;

import com.rabbit.entity.Area;

public class AreaDto extends Area {
    private String custName;

    private Long pintaiNum;

    private Long plcNum;

    private Long bonNum;

    private Long jinNum;

    private Long ljbuyeNum;

    private Long ljxunjianNum;

    private Long ljsuyanjianceNum;

    private Long ljsixiaojiluNum;

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public Long getJinNum() {
        return jinNum;
    }

    public void setJinNum(Long jinNum) {
        this.jinNum = jinNum;
    }

    public Long getPintaiNum() {
        return pintaiNum;
    }

    public void setPintaiNum(Long pintaiNum) {
        this.pintaiNum = pintaiNum;
    }

    public Long getPlcNum() {
        return plcNum;
    }

    public void setPlcNum(Long plcNum) {
        this.plcNum = plcNum;
    }

    public Long getBonNum() {
        return bonNum;
    }

    public void setBonNum(Long bonNum) {
        this.bonNum = bonNum;
    }

    public Long getLjbuyeNum() {
        return ljbuyeNum;
    }

    public void setLjbuyeNum(Long ljbuyeNum) {
        this.ljbuyeNum = ljbuyeNum;
    }

    public Long getLjxunjianNum() {
        return ljxunjianNum;
    }

    public void setLjxunjianNum(Long ljxunjianNum) {
        this.ljxunjianNum = ljxunjianNum;
    }

    public Long getLjsuyanjianceNum() {
        return ljsuyanjianceNum;
    }

    public void setLjsuyanjianceNum(Long ljsuyanjianceNum) {
        this.ljsuyanjianceNum = ljsuyanjianceNum;
    }

    public Long getLjsixiaojiluNum() {
        return ljsixiaojiluNum;
    }

    public void setLjsixiaojiluNum(Long ljsixiaojiluNum) {
        this.ljsixiaojiluNum = ljsixiaojiluNum;
    }
}
