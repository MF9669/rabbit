package com.rabbit.dto.area;

import java.util.Date;

public class AreaSearchDto {
    private int pageNum = 1;

    private int pageSize = 10;

    private String quyuname;

    private Long custId;//根据custId查，并不是custName

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    private String guishuname;

    private Boolean inService;

    private Date lo;

    private Date hi;

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "AreaSearchDto{" +
                "quyuname='" + quyuname + '\'' +
                ", guishuname='" + guishuname + '\'' +
                ", inService=" + inService +
                ", lo=" + lo +
                ", hi=" + hi +
                '}';
    }

    public String getQuyuname() {
        return quyuname;
    }

    public void setQuyuname(String quyuname) {
        this.quyuname = quyuname;
    }

    public String getGuishuname() {
        return guishuname;
    }

    public void setGuishuname(String guishuname) {
        this.guishuname = guishuname;
    }

    public Boolean getInService() {
        return inService;
    }

    public void setInService(Boolean inService) {
        this.inService = inService;
    }

    public Date getLo() {
        return lo;
    }

    public void setLo(Date lo) {
        this.lo = lo;
    }

    public Date getHi() {
        return hi;
    }

    public void setHi(Date hi) {
        this.hi = hi;
    }
}
