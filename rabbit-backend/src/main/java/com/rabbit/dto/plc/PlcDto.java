package com.rabbit.dto.plc;

import com.rabbit.entity.Plc;

public class PlcDto extends Plc {
    private String custName;
    private String areaName;
    private String stationName;
    private Long pumpNum;
    private Long wellNum;

    private Long replenishNum;
    private Long checkNum;
    private Long waterTestNum;
    private Long outNum;

    public Long getReplenishNum() {
        return replenishNum;
    }

    public void setReplenishNum(Long replenishNum) {
        this.replenishNum = replenishNum;
    }

    public Long getCheckNum() {
        return checkNum;
    }

    public void setCheckNum(Long checkNum) {
        this.checkNum = checkNum;
    }

    public Long getWaterTestNum() {
        return waterTestNum;
    }

    public void setWaterTestNum(Long waterTestNum) {
        this.waterTestNum = waterTestNum;
    }

    public Long getOutNum() {
        return outNum;
    }

    public void setOutNum(Long outNum) {
        this.outNum = outNum;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public Long getPumpNum() {
        return pumpNum;
    }

    public void setPumpNum(Long pumpNum) {
        this.pumpNum = pumpNum;
    }

    public Long getWellNum() {
        return wellNum;
    }

    public void setWellNum(Long wellNum) {
        this.wellNum = wellNum;
    }
}
