package com.rabbit.dto.plc;

import java.util.Date;

public class PlcSearchDto {
    private int pageNum=1;
    private int pageSize=10;
    private String name;
    private String custName;
    private String areaName;
    private String stationName;
    private Boolean online;
    private String businessType ;
    private String workMode;

    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getWorkMode() {
        return workMode;
    }

    public void setWorkMode(String workMode) {
        this.workMode = workMode;
    }

    private Date lo;
    private Date hi;

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public Date getLo() {
        return lo;
    }

    public void setLo(Date lo) {
        this.lo = lo;
    }

    public Date getHi() {
        return hi;
    }

    public void setHi(Date hi) {
        this.hi = hi;
    }


    @Override
    public String toString() {
        return "PlcSearchDto{" +
                "pageNum=" + pageNum +
                ", pageSize=" + pageSize +
                ", name='" + name + '\'' +
                ", custName='" + custName + '\'' +
                ", areaName='" + areaName + '\'' +
                ", stationName='" + stationName + '\'' +
                ", lo=" + lo +
                ", hi=" + hi +
                '}';
    }
}
