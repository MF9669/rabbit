package com.rabbit.dto.station;

import com.rabbit.entity.Station;

import java.util.Date;

public class StationDto extends Station {

    private String custName;
    private String areaName;
    private Long PLCNum;
    private Long pumpNum;
    private Long wellNum;
    private Long replenishNum;
    private Long checkNum;
    private Long waterTestNum;
    private Long outNum;

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public Long getPLCNum() {
        return PLCNum;
    }

    public void setPLCNum(Long PLCNum) {
        this.PLCNum = PLCNum;
    }

    public Long getPumpNum() {
        return pumpNum;
    }

    public void setPumpNum(Long pumpNum) {
        this.pumpNum = pumpNum;
    }

    public Long getWellNum() {
        return wellNum;
    }

    public void setWellNum(Long wellNum) {
        this.wellNum = wellNum;
    }

    public Long getReplenishNum() {
        return replenishNum;
    }

    public void setReplenishNum(Long replenishNum) {
        this.replenishNum = replenishNum;
    }

    public Long getCheckNum() {
        return checkNum;
    }

    public void setCheckNum(Long checkNum) {
        this.checkNum = checkNum;
    }

    public Long getWaterTestNum() {
        return waterTestNum;
    }

    public void setWaterTestNum(Long waterTestNum) {
        this.waterTestNum = waterTestNum;
    }

    public Long getOutNum() {
        return outNum;
    }

    public void setOutNum(Long outNum) {
        this.outNum = outNum;
    }

    @Override
    public String toString() {
        return "StationDto{" +
                "custName='" + custName + '\'' +
                ", areaName='" + areaName + '\'' +
                ", PLCNum=" + PLCNum +
                ", pumpNum=" + pumpNum +
                ", wellNum=" + wellNum +
                ", replenishNum=" + replenishNum +
                ", checkNum=" + checkNum +
                ", waterTestNum=" + waterTestNum +
                ", outNum=" + outNum +
                '}';

}
}
