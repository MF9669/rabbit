package com.rabbit.dto.station;

import java.util.Date;

public class StationSearchDto {

    private int pageNum=1;
    private int pageSize=10;
    private String name;
    private String custName;
    private String areaName;
    private Boolean inService;
    private Date low;
    private Date high;

    //补充：custId,areaId
    private Long custId;
    private Long areaId;

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public Boolean getInService() {
        return inService;
    }

    public void setInService(Boolean inService) {
        this.inService = inService;
    }

    public Date getLow() {
        return low;
    }

    public void setLow(Date low) {
        this.low = low;
    }

    public Date getHigh() {
        return high;
    }

    public void setHigh(Date high) {
        this.high = high;
    }
}
