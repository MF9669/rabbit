package com.rabbit.dto.warn;

import com.rabbit.entity.Warn;

public class WarnDto extends Warn {
    private String custName;
    private String areaName;
    private String stationName;
    private String PLCName;
    private String pumpName;

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getPLCName() {
        return PLCName;
    }

    public void setPLCName(String PLCName) {
        this.PLCName = PLCName;
    }

    public String getPumpName() {
        return pumpName;
    }

    public void setPumpName(String pumpName) {
        this.pumpName = pumpName;
    }

    @Override
    public String toString() {
        return "WarnDto{" +
                "custName='" + custName + '\'' +
                ", areaName='" + areaName + '\'' +
                ", stationName='" + stationName + '\'' +
                ", PLCName='" + PLCName + '\'' +
                ", pumpName='" + pumpName + '\'' +
                '}';
    }
}
