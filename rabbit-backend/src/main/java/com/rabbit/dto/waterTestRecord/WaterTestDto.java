package com.rabbit.dto.waterTestRecord;

import com.rabbit.entity.WaterTestRecord;

import java.util.Date;

public class WaterTestDto extends WaterTestRecord {
    private String custName;
    private String areaName;
    private String stationName;

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }
}
