package com.rabbit.dto.waterTestRecord;

import java.util.Date;

public class WaterTestSearchDto {
    private int pageNum=1;
    private int pageSize=10;
    private String custName;
    private String areaName;
    private String stationName;
    private Date low;
    private Date high;

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public Date getLow() {
        return low;
    }

    public void setLow(Date low) {
        this.low = low;
    }

    public Date getHigh() {
        return high;
    }

    public void setHigh(Date high) {
        this.high = high;
    }
}
