package com.rabbit.dto.well;

import com.rabbit.entity.Well;

import java.util.Date;

public class WellDto extends Well {

    private String name;
    private String custName;
    private String areaName;
    private String stationName;
    private String PLCName;
    private String pumpName;

    private Boolean onOff;

    private Date installDate;

    private Date useDate;

    private Date zjbyrq;

    private String createBy;

    private Date createTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getPLCName() {
        return PLCName;
    }

    public void setPLCName(String PLCName) {
        this.PLCName = PLCName;
    }

    public String getPumpName() {
        return pumpName;
    }

    public void setPumpName(String pumpName) {
        this.pumpName = pumpName;
    }

    public Boolean getOnOff() {
        return onOff;
    }

    public void setOnOff(Boolean onOff) {
        this.onOff = onOff;
    }

    public Date getInstallDate() {
        return installDate;
    }

    public void setInstallDate(Date installDate) {
        this.installDate = installDate;
    }

    public Date getUseDate() {
        return useDate;
    }

    public void setUseDate(Date useDate) {
        this.useDate = useDate;
    }

    public Date getZjbyrq() {
        return zjbyrq;
    }

    public void setZjbyrq(Date zjbyrq) {
        this.zjbyrq = zjbyrq;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "WellDto{" +
                "custName='" + custName + '\'' +
                ", areaName='" + areaName + '\'' +
                ", stationName='" + stationName + '\'' +
                ", PLCName='" + PLCName + '\'' +
                ", pumpName='" + pumpName + '\'' +
                ", onOff=" + onOff +
                ", installDate=" + installDate +
                ", useDate=" + useDate +
                ", zjbyrq=" + zjbyrq +
                ", createBy='" + createBy + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
