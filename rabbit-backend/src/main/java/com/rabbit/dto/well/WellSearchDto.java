package com.rabbit.dto.well;

import java.util.Date;

public class WellSearchDto {
    private int pageNum=1;
    private int pageSize=10;
    private String name;
    private String custName;
    private String areaName;
    private String stationName;
    private String PLCName;
    private String pumpName;
    private Boolean onOff;
    private Date low;
    private Date high;

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getPLCName() {
        return PLCName;
    }

    public void setPLCName(String PLCName) {
        this.PLCName = PLCName;
    }

    public String getPumpName() {
        return pumpName;
    }

    public void setPumpName(String pumpName) {
        this.pumpName = pumpName;
    }

    public Boolean getOnOff() {
        return onOff;
    }

    public void setOnOff(Boolean onOff) {
        this.onOff = onOff;
    }

    public Date getLow() {
        return low;
    }

    public void setLow(Date low) {
        this.low = low;
    }

    public Date getHigh() {
        return high;
    }

    public void setHigh(Date high) {
        this.high = high;
    }
}
