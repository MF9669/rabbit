package com.rabbit.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 巡检记录
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@TableName("check_record")
public class CheckRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * FK: 所属客户id
     */
    private Long custId;

    /**
     * FK: 所属区域id
     */
    private Long areaId;

    /**
     * FK: 所属场站id
     */
    private Long stationId;

    /**
     * FK: 所属PLC id
     */
    private Long plcId;

    /**
     * FK: 所属泵 id
     */
    private Long pumpId;

    /**
     * FK: 所属井 id
     */
    private Long wellId;

    /**
     * 巡检日期
     */
    private Date checkDate;

    /**
     * 泵注速度
     */
    private Double pumpSpeed;

    /**
     * 巡检内容说明
     */
    private String checkMsg;

    /**
     * 场地清理情况
     */
    private String cleanMsg;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 修改人
     */
    private String updateBy;

    /**
     * 逻辑删除标志：0=未删除，1=已删除
     */
    @TableLogic
    private Boolean delFlag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }
    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }
    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }
    public Long getPlcId() {
        return plcId;
    }

    public void setPlcId(Long plcId) {
        this.plcId = plcId;
    }
    public Long getPumpId() {
        return pumpId;
    }

    public void setPumpId(Long pumpId) {
        this.pumpId = pumpId;
    }
    public Long getWellId() {
        return wellId;
    }

    public void setWellId(Long wellId) {
        this.wellId = wellId;
    }
    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }
    public Double getPumpSpeed() {
        return pumpSpeed;
    }

    public void setPumpSpeed(Double pumpSpeed) {
        this.pumpSpeed = pumpSpeed;
    }
    public String getCheckMsg() {
        return checkMsg;
    }

    public void setCheckMsg(String checkMsg) {
        this.checkMsg = checkMsg;
    }
    public String getCleanMsg() {
        return cleanMsg;
    }

    public void setCleanMsg(String cleanMsg) {
        this.cleanMsg = cleanMsg;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
    public Boolean getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Boolean delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "CheckRecord{" +
            "id=" + id +
            ", custId=" + custId +
            ", areaId=" + areaId +
            ", stationId=" + stationId +
            ", plcId=" + plcId +
            ", pumpId=" + pumpId +
            ", wellId=" + wellId +
            ", checkDate=" + checkDate +
            ", pumpSpeed=" + pumpSpeed +
            ", checkMsg=" + checkMsg +
            ", cleanMsg=" + cleanMsg +
            ", remark=" + remark +
            ", createTime=" + createTime +
            ", createBy=" + createBy +
            ", updateTime=" + updateTime +
            ", updateBy=" + updateBy +
            ", delFlag=" + delFlag +
        "}";
    }
}
