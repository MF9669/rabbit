package com.rabbit.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public class Plc implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * FK: 所属客户id
     */
    private Long custId;

    /**
     * FK: 所属区域id
     */
    private Long areaId;

    /**
     * FK: 所属场站id
     */
    private Long stationId;

    /**
     * PLC名称
     */
    private String name;

    /**
     * 在线状态：0=离线，1=在线
     */
    private Boolean online;

    /**
     * 业务类别：防腐/泡排
     */
    private String businessType;

    /**
     * 工作模式：单泵/撬装/一泵多注/撬装一泵多注
     */
    private String workMode;

    /**
     * 数据中心ip地址
     */
    private String ip;

    /**
     * 数据中心端口
     */
    private Integer port;

    /**
     * 数据中心密码
     */
    private String pwd;

    /**
     * 数据块首地址
     */
    private Integer headAddr;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 修改人
     */
    private String updateBy;

    /**
     * 逻辑删除标志：0=未删除，1=已删除
     */
    @TableLogic
    private Boolean delFlag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }
    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }
    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }
    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }
    public String getWorkMode() {
        return workMode;
    }

    public void setWorkMode(String workMode) {
        this.workMode = workMode;
    }
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
    public Integer getHeadAddr() {
        return headAddr;
    }

    public void setHeadAddr(Integer headAddr) {
        this.headAddr = headAddr;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
    public Boolean getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Boolean delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "Plc{" +
            "id=" + id +
            ", custId=" + custId +
            ", areaId=" + areaId +
            ", stationId=" + stationId +
            ", name=" + name +
            ", online=" + online +
            ", businessType=" + businessType +
            ", workMode=" + workMode +
            ", ip=" + ip +
            ", port=" + port +
            ", pwd=" + pwd +
            ", headAddr=" + headAddr +
            ", remark=" + remark +
            ", createTime=" + createTime +
            ", createBy=" + createBy +
            ", updateTime=" + updateTime +
            ", updateBy=" + updateBy +
            ", delFlag=" + delFlag +
        "}";
    }
}
