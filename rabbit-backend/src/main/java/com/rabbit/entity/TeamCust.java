package com.rabbit.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
 * <p>
 * 队伍和客户的关系表
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@TableName("team_cust")
public class TeamCust implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 队伍id
     */
    private Long teamId;

    /**
     * 客户id
     */
    private Long custId;

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }
    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    @Override
    public String toString() {
        return "TeamCust{" +
            "teamId=" + teamId +
            ", custId=" + custId +
        "}";
    }
}
