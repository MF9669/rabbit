package com.rabbit.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 水样检测记录
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@TableName("water_test_record")
public class WaterTestRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * FK: 所属客户id
     */
    private Long custId;

    /**
     * FK: 所属区域id
     */
    private Long areaId;

    /**
     * FK: 所属场站id
     */
    private Long stationId;

    /**
     * 取样日期
     */
    private Date sampleDate;

    /**
     * 取样位置
     */
    private String samplePos;

    /**
     * 检测日期
     */
    private Date testDate;

    /**
     * 实验员
     */
    private String testBy;

    /**
     * SRB检测值
     */
    private Integer srb;

    /**
     * TGB检测值
     */
    private Integer tgb;

    /**
     * FB检测值
     */
    private Integer fb;

    /**
     * 菌落数
     */
    private Integer cfu;

    /**
     * 总铁数
     */
    private Integer ppm;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 修改人
     */
    private String updateBy;

    /**
     * 逻辑删除标志：0=未删除，1=已删除
     */
    @TableLogic
    private Boolean delFlag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }
    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }
    public Long getStationId() {
        return stationId;
    }

    public void setStationId(Long stationId) {
        this.stationId = stationId;
    }
    public Date getSampleDate() {
        return sampleDate;
    }

    public void setSampleDate(Date sampleDate) {
        this.sampleDate = sampleDate;
    }
    public String getSamplePos() {
        return samplePos;
    }

    public void setSamplePos(String samplePos) {
        this.samplePos = samplePos;
    }
    public Date getTestDate() {
        return testDate;
    }

    public void setTestDate(Date testDate) {
        this.testDate = testDate;
    }
    public String getTestBy() {
        return testBy;
    }

    public void setTestBy(String testBy) {
        this.testBy = testBy;
    }
    public Integer getSrb() {
        return srb;
    }

    public void setSrb(Integer srb) {
        this.srb = srb;
    }
    public Integer getTgb() {
        return tgb;
    }

    public void setTgb(Integer tgb) {
        this.tgb = tgb;
    }
    public Integer getFb() {
        return fb;
    }

    public void setFb(Integer fb) {
        this.fb = fb;
    }
    public Integer getCfu() {
        return cfu;
    }

    public void setCfu(Integer cfu) {
        this.cfu = cfu;
    }
    public Integer getPpm() {
        return ppm;
    }

    public void setPpm(Integer ppm) {
        this.ppm = ppm;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
    public Boolean getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Boolean delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "WaterTestRecord{" +
            "id=" + id +
            ", custId=" + custId +
            ", areaId=" + areaId +
            ", stationId=" + stationId +
            ", sampleDate=" + sampleDate +
            ", samplePos=" + samplePos +
            ", testDate=" + testDate +
            ", testBy=" + testBy +
            ", srb=" + srb +
            ", tgb=" + tgb +
            ", fb=" + fb +
            ", cfu=" + cfu +
            ", ppm=" + ppm +
            ", remark=" + remark +
            ", createTime=" + createTime +
            ", createBy=" + createBy +
            ", updateTime=" + updateTime +
            ", updateBy=" + updateBy +
            ", delFlag=" + delFlag +
        "}";
    }
}
