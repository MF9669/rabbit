package com.rabbit.mapper;

import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.area.AreaDto;
import com.rabbit.dto.area.AreaSearchDto;
import com.rabbit.entity.Area;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface AreaMapper extends BaseMapper<Area> {
    List<AreaDto> search(AreaSearchDto areaSearchDto);
}
