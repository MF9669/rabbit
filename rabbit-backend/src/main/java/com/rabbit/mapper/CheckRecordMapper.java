package com.rabbit.mapper;

import com.rabbit.dto.CheckRecordDto;
import com.rabbit.dto.CheckRecordSearchDto;
import com.rabbit.entity.CheckRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 巡检记录 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface CheckRecordMapper extends BaseMapper<CheckRecord> {

    List<CheckRecordDto> search(CheckRecordSearchDto checkRecordSearchDto);
}
