package com.rabbit.mapper;

import com.rabbit.dto.Cust.CustDto;
import com.rabbit.dto.Cust.CustSearchDto;
import com.rabbit.entity.Cust;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 客户表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface CustMapper extends BaseMapper<Cust> {

    List<CustDto> search(CustSearchDto searchDto);
}
