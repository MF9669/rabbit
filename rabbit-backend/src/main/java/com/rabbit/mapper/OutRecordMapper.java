package com.rabbit.mapper;

import com.rabbit.entity.OutRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 失效记录 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface OutRecordMapper extends BaseMapper<OutRecord> {

}
