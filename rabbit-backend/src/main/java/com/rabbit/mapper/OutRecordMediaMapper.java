package com.rabbit.mapper;

import com.rabbit.entity.OutRecordMedia;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 失效记录的图片和视频 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface OutRecordMediaMapper extends BaseMapper<OutRecordMedia> {

}
