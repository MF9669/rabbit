package com.rabbit.mapper;

import com.rabbit.dto.plc.PlcDto;
import com.rabbit.dto.plc.PlcSearchDto;
import com.rabbit.entity.Plc;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface PlcMapper extends BaseMapper<Plc> {
    List<PlcDto> search(PlcSearchDto plcSearch);
}
