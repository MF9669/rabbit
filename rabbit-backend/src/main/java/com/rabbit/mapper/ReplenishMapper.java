package com.rabbit.mapper;

import com.rabbit.entity.Replenish;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 补液记录 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface ReplenishMapper extends BaseMapper<Replenish> {

}
