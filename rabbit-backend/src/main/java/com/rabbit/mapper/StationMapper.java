package com.rabbit.mapper;

import com.rabbit.dto.station.StationDto;
import com.rabbit.dto.station.StationSearchDto;
import com.rabbit.entity.Station;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface StationMapper extends BaseMapper<Station> {

    List<StationDto> search(StationSearchDto searchDto);
}
