package com.rabbit.mapper;

import com.rabbit.entity.Team;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface TeamMapper extends BaseMapper<Team> {

    List<Team> search(String name);
}
