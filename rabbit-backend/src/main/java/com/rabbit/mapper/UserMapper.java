package com.rabbit.mapper;

import com.rabbit.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface UserMapper extends BaseMapper<User> {

}
