package com.rabbit.mapper;

import com.rabbit.dto.warn.WarnDto;
import com.rabbit.dto.warn.WarnSearchDto;
import com.rabbit.entity.Warn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 报警记录 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface WarnMapper extends BaseMapper<Warn> {


    List<WarnDto> search(WarnSearchDto warnSearchDto);
}

