package com.rabbit.mapper;

import com.rabbit.dto.waterTestRecord.WaterTestDto;
import com.rabbit.dto.waterTestRecord.WaterTestSearchDto;
import com.rabbit.entity.WaterTestRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 水样检测记录 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface WaterTestRecordMapper extends BaseMapper<WaterTestRecord> {

    List<WaterTestDto> search(WaterTestSearchDto waterTestSearchDto);
}
