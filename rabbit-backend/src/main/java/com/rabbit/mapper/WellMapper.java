package com.rabbit.mapper;

import com.rabbit.dto.well.WellDto;
import com.rabbit.dto.well.WellSearchDto;
import com.rabbit.entity.Well;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface WellMapper extends BaseMapper<Well> {

    List<WellDto> search(WellSearchDto wellSearchDto);
}
