package com.rabbit.service;

import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.area.AreaDto;
import com.rabbit.dto.area.AreaSearchDto;
import com.rabbit.entity.Area;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface IAreaService extends IService<Area> {
    PageSerializable<AreaDto> search(AreaSearchDto areaSearchDto);
}
