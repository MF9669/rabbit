package com.rabbit.service;

import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.CheckRecordDto;
import com.rabbit.dto.CheckRecordSearchDto;
import com.rabbit.entity.CheckRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 巡检记录 服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface ICheckRecordService extends IService<CheckRecord> {

    PageSerializable<CheckRecordDto> search(CheckRecordSearchDto checkRecordSearchDto);
}
