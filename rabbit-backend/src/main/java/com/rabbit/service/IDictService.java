package com.rabbit.service;

import com.github.pagehelper.PageSerializable;
import com.rabbit.entity.Dict;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.DeleteProvider;

import java.util.List;

/**
 * <p>
 * 数据字典 服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface IDictService extends IService<Dict> {

    List<Dict> search(String name);
}
