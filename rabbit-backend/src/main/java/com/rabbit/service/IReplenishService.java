package com.rabbit.service;

import com.rabbit.entity.Replenish;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 补液记录 服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface IReplenishService extends IService<Replenish> {

}
