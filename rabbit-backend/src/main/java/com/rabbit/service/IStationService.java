package com.rabbit.service;

import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.station.StationDto;
import com.rabbit.dto.station.StationSearchDto;
import com.rabbit.entity.Station;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface IStationService extends IService<Station> {

    PageSerializable<StationDto> search(StationSearchDto stationSearchDto);
}
