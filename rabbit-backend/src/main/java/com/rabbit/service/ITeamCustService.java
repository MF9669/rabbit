package com.rabbit.service;

import com.rabbit.entity.TeamCust;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 队伍和客户的关系表 服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface ITeamCustService extends IService<TeamCust> {

}
