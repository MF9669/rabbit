package com.rabbit.service;

import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.waterTestRecord.WaterTestDto;
import com.rabbit.dto.waterTestRecord.WaterTestSearchDto;
import com.rabbit.entity.WaterTestRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 水样检测记录 服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface IWaterTestRecordService extends IService<WaterTestRecord> {

    PageSerializable<WaterTestDto> search(WaterTestSearchDto waterTestSearchDto);
}
