package com.rabbit.service;

import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.well.WellDto;
import com.rabbit.dto.well.WellSearchDto;
import com.rabbit.entity.Well;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
public interface IWellService extends IService<Well> {

    PageSerializable<WellDto> search(WellSearchDto wellSearchDto);
}
