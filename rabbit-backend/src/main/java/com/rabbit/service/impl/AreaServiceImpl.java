package com.rabbit.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.area.AreaDto;
import com.rabbit.dto.area.AreaSearchDto;
import com.rabbit.entity.Area;
import com.rabbit.mapper.AreaMapper;
import com.rabbit.service.IAreaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class AreaServiceImpl extends ServiceImpl<AreaMapper, Area> implements IAreaService {
    @Override
    public PageSerializable<AreaDto> search(AreaSearchDto areaSearchDto) {
        PageHelper.startPage(areaSearchDto.getPageNum(),areaSearchDto.getPageSize());
        List<AreaDto> list = this.getBaseMapper().search(areaSearchDto);
        return PageSerializable.of(list);
    }
}
