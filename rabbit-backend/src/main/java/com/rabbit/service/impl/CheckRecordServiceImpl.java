package com.rabbit.service.impl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.CheckRecordDto;
import com.rabbit.dto.CheckRecordSearchDto;
import com.rabbit.entity.CheckRecord;
import com.rabbit.mapper.CheckRecordMapper;
import com.rabbit.service.ICheckRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 巡检记录 服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class CheckRecordServiceImpl extends ServiceImpl<CheckRecordMapper, CheckRecord> implements ICheckRecordService {
    @Override
    public PageSerializable<CheckRecordDto> search(CheckRecordSearchDto checkRecordSearchDto) {
        PageHelper.startPage(checkRecordSearchDto.getPageNum(),checkRecordSearchDto.getPageSize());
        List<CheckRecordDto> list = this.getBaseMapper().search(checkRecordSearchDto);
        return PageSerializable.of(list);
    }
}
