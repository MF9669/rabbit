package com.rabbit.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.Cust.CustDto;
import com.rabbit.dto.Cust.CustSearchDto;
import com.rabbit.entity.Cust;
import com.rabbit.mapper.CustMapper;
import com.rabbit.service.ICustService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 客户表 服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class CustServiceImpl extends ServiceImpl<CustMapper, Cust> implements ICustService {

    @Override
    public PageSerializable<CustDto> search(CustSearchDto searchDto) {
        PageHelper.startPage(searchDto.getPageNum(), searchDto.getPageSize());
        List<CustDto> list= this.getBaseMapper().search(searchDto);

        return PageSerializable.of(list);
    }
}
