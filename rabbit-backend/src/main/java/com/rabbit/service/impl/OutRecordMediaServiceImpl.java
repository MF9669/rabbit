package com.rabbit.service.impl;

import com.rabbit.entity.OutRecordMedia;
import com.rabbit.mapper.OutRecordMediaMapper;
import com.rabbit.service.IOutRecordMediaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 失效记录的图片和视频 服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class OutRecordMediaServiceImpl extends ServiceImpl<OutRecordMediaMapper, OutRecordMedia> implements IOutRecordMediaService {

}
