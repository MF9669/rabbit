package com.rabbit.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.plc.PlcDto;
import com.rabbit.dto.plc.PlcSearchDto;
import com.rabbit.entity.Plc;
import com.rabbit.mapper.PlcMapper;
import com.rabbit.service.IPlcService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class PlcServiceImpl extends ServiceImpl<PlcMapper, Plc> implements IPlcService {

//    @Autowired
//    PlcMapper plcMapper;
//    不用注入，父类已经注入了。除非你需要用到其他表的mapper
    @Override
    public PageSerializable<PlcDto> search(PlcSearchDto plcSearch) {
        PageHelper.startPage(plcSearch.getPageNum(), plcSearch.getPageSize());
        List<PlcDto> list= this.getBaseMapper().search(plcSearch);
        return PageSerializable.of(list);
    }
}
