package com.rabbit.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.station.StationDto;
import com.rabbit.dto.station.StationSearchDto;
import com.rabbit.entity.Station;
import com.rabbit.mapper.StationMapper;
import com.rabbit.service.IStationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class StationServiceImpl extends ServiceImpl<StationMapper, Station> implements IStationService {

    @Override
    public PageSerializable<StationDto> search(StationSearchDto searchDto) {
        PageHelper.startPage(searchDto.getPageNum(),searchDto.getPageSize());
        List<StationDto> list = this.getBaseMapper().search(searchDto);
        return PageSerializable.of(list);
    }
}
