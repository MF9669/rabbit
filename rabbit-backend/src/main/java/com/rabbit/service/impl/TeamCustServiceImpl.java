package com.rabbit.service.impl;

import com.rabbit.entity.TeamCust;
import com.rabbit.mapper.TeamCustMapper;
import com.rabbit.service.ITeamCustService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 队伍和客户的关系表 服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class TeamCustServiceImpl extends ServiceImpl<TeamCustMapper, TeamCust> implements ITeamCustService {

}
