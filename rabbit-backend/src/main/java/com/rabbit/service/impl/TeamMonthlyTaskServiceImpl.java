package com.rabbit.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.TeamMonthlyTask.TeamMonthlyTaskSearchDto;
import com.rabbit.entity.TeamMonthlyTask;
import com.rabbit.mapper.TeamMonthlyTaskMapper;
import com.rabbit.service.ITeamMonthlyTaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 队伍月度任务 服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class TeamMonthlyTaskServiceImpl extends ServiceImpl<TeamMonthlyTaskMapper, TeamMonthlyTask> implements ITeamMonthlyTaskService {

    @Override
    public PageSerializable<TeamMonthlyTask> search(TeamMonthlyTaskSearchDto searchDto) {
        PageHelper.startPage(searchDto.getPageNum(),searchDto.getPageSize());
        List<TeamMonthlyTask> list = this.getBaseMapper().search( searchDto);
        return PageSerializable.of(list);
    }
}
