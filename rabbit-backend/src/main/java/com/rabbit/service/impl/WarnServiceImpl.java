package com.rabbit.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.warn.WarnDto;
import com.rabbit.dto.warn.WarnSearchDto;
import com.rabbit.entity.Warn;
import com.rabbit.mapper.WarnMapper;
import com.rabbit.service.IWarnService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 报警记录 服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class WarnServiceImpl extends ServiceImpl<WarnMapper, Warn> implements IWarnService {


    @Override
    public PageSerializable<WarnDto> search(WarnSearchDto warnSearchDto) {
        PageHelper.startPage(warnSearchDto.getPageNum(), warnSearchDto.getPageSize());
        List<WarnDto> list = (List<WarnDto>) this.getBaseMapper().search(warnSearchDto);
        return PageSerializable.of(list);
    }
}
