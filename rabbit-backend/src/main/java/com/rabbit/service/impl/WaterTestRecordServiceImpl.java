package com.rabbit.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.waterTestRecord.WaterTestDto;
import com.rabbit.dto.waterTestRecord.WaterTestSearchDto;
import com.rabbit.entity.WaterTestRecord;
import com.rabbit.mapper.WaterTestRecordMapper;
import com.rabbit.service.IWaterTestRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 水样检测记录 服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class WaterTestRecordServiceImpl extends ServiceImpl<WaterTestRecordMapper, WaterTestRecord> implements IWaterTestRecordService {

    @Override
    public PageSerializable<WaterTestDto> search(WaterTestSearchDto waterTestSearchDto) {
        PageHelper.startPage(waterTestSearchDto.getPageNum(), waterTestSearchDto.getPageSize());
        List<WaterTestDto> list = this.getBaseMapper().search(waterTestSearchDto);
        return PageSerializable.of(list);
    }
}
