package com.rabbit.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageSerializable;
import com.rabbit.dto.well.WellDto;
import com.rabbit.dto.well.WellSearchDto;
import com.rabbit.entity.Well;
import com.rabbit.mapper.WellMapper;
import com.rabbit.service.IWellService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2023-01-30
 */
@Service
public class WellServiceImpl extends ServiceImpl<WellMapper, Well> implements IWellService {

    @Override
    public PageSerializable<WellDto> search(WellSearchDto wellSearchDto) {
        PageHelper.startPage(wellSearchDto.getPageNum(),wellSearchDto.getPageSize());
        List<WellDto> list = this.getBaseMapper().search(wellSearchDto);
        return PageSerializable.of(list);
    }
}
