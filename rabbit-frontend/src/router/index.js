import Vue from "vue";
import VueRouter from "vue-router";
// import HomeView from "../views/HomeView.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/login/index"),
  },
  {
    path: "/",
    redirect: "/one/page1", //重定向，使有一个默认页面
    name: "Layout",
    component: () => import("../layout/index"),
    // Layout的子组件Main会显示具体各个页面
    children: [
      {
        path: "/one/page1",
        name: "One_Page1",
        component: () => import("../views/one/page1/index"),
      },
      {
        path: "/one/page2",
        name: "One_Page2",
        component: () => import("../views/one/page2/index"),
      },
      {
        path: "/one/page3",
        name: "One_Page3",
        component: () => import("../views/one/page3/index"),
      },
      {
        path: "/one/page4",
        name: "One_Page4",
        component: () => import("../views/one/page4/index"),
      },
      {
        path: "/one/page5",
        name: "One_Page5",
        component: () => import("../views/one/page5/index"),
      },
      {
        path: "/one/page6",
        name: "One_Page6",
        component: () => import("../views/one/page6/index"),
      },
      {
        path: "/one/page7",
        name: "One_Page7",
        component: () => import("../views/one/page7/index"),
      },
      {
        path: "/one/page8",
        name: "One_Page8",
        component: () => import("../views/one/page8/index"),
      },
      {
        path: "/one/page9",
        name: "One_Page9",
        component: () => import("../views/one/page9/index"),
      },
      {
        path: "/one/page11",
        name: "One_Page11",
        component: () => import("../views/one/page11/index"),
      },
      {
        path: "/one/page12",
        name: "One_Page12",
        component: () => import("../views/one/page12/index")
      },
      {
        path: "/one/page13",
        name: "One_Page13",
        component: () => import("../views/one/page13/index")
      },
      {
        path: "/one/pagex",
        name: "One_Pagex",
        component: () => import("../views/one/pagex/index")
      },
      // <-------------------------------->
      {
        path: "/two/page1",
        name: "Two_Page1",
        component: () => import("../views/two/page1/index"),
      },
      {
        path: "/two/page2",
        name: "Two_Page2",
        component: () => import("../views/two/page2/index"),
      },
    ],
  },
];

const router = new VueRouter({
  routes,
});

export default router;
